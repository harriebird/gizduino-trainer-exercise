#include <LEDDisplay.h>

LEDDisplay *led;
int tempPin = A0;


void setup() {
  Serial.begin(9600);
  int digitFlagPins[] = {10, 11};
  int segmentPins[] = {2, 3, 4, 5 ,6 ,7 ,8, 9};
  int decimalPointPin = 9;
  led = new LEDDisplay(2, digitFlagPins, segmentPins, decimalPointPin);
}

void loop() {
  int rawVoltage = analogRead(tempPin);
  float milliVolts = (rawVoltage /1024.0) * 5000;
  float fahrenheit= milliVolts / 10;
  Serial.print(fahrenheit);
  Serial.println(" degrees Fahrenheit, ");
  
  float celsius = (fahrenheit - 32) * (5.0/9.0);
  
  Serial.print (celsius);
  Serial.println(" degrees Celsius");
  int wholeTemp = int(celsius);

  int tens = int(wholeTemp / 10);
  int ones = wholeTemp % 10;
  
  Serial.println("------");
  Serial.println(tens);
  Serial.println(ones);
  Serial.println("------");
  
  delay(5000);
  led->displayNumber(ones, 1);
  led->displayNumber(tens, 0);
}
